package starter.tasks.categories;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import starter.tasks.categories.DoCategories;
import starter.ui.categories.CatgoriesForm;
//import starter.ui.categories.CatgoriesForm;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class DoCategories implements Task {

    private final String name_category;
    private final String name_vietnamese;
    private final String name_russian;
    private final String name_arabic;
    private final String name_farsi;
    private final String name_turkish;
    private final String name_french;
    private final String name_spanish;
    private final String name_german;


    public DoCategories(String name_category, String name_vietnamese, String name_russian, String name_arabic,String name_farsi, String name_turkish, String name_french, String name_spanish, String name_german) {
        this.name_category = name_category;
        this.name_vietnamese = name_vietnamese;
        this.name_russian = name_russian;
        this.name_arabic = name_arabic;
        this.name_farsi = name_farsi;
        this.name_turkish = name_turkish;
        this.name_french = name_french;
        this.name_spanish = name_spanish;
        this.name_german = name_german;

    }

    public static Performable withCategory(String name_category, String name_vietnamese, String name_russian, String name_arabic,String name_farsi, String name_turkish, String name_french, String name_spanish, String name_german) {
        return instrumented(DoCategories.class, name_category, name_vietnamese,name_russian, name_arabic, name_farsi , name_turkish, name_french, name_spanish, name_german);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(name_category).into(CatgoriesForm.NAME_CATEGORY),
                Enter.theValue(name_vietnamese).into(CatgoriesForm.NAME_VIETNAMESE),
                Enter.theValue(name_russian).into(CatgoriesForm.NAME_RUSSIAN),
                Enter.theValue(name_arabic).into(CatgoriesForm.NAME_ARABIC),
                Enter.theValue(name_farsi).into(CatgoriesForm.NAME_FARSI),
                Enter.theValue(name_turkish).into(CatgoriesForm.NAME_TURKISH),
                Enter.theValue(name_french).into(CatgoriesForm.NAME_FRENCH),
                Enter.theValue(name_spanish).into(CatgoriesForm.NAME_SPANISH),
                Enter.theValue(name_german).into(CatgoriesForm.NAME_GERMAN),
                Click.on(CatgoriesForm.BUTTON_SAVE)

        );
    }
}

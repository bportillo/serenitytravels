package starter.tasks.typeflight;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import starter.ui.form.Form;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class DoDateFlight implements Task {

    public DoDateFlight() {

    }

    public static Performable withDate() {
        return instrumented(DoDateFlight.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(Form.DATESELECT)
        );
    }
}

package starter.tasks.typeflight;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import starter.tasks.login.DoLogin;
import starter.ui.form.Form;
import starter.ui.login.LoginForm;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class DoTypeFlight implements Task {

    public DoTypeFlight() {

    }

    public static Performable withtype() {
        return instrumented(DoTypeFlight.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(Form.TYPEFLIGHT)
        );
    }
}

package starter.tasks.typeflight;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import starter.tasks.login.DoLogin;
import starter.ui.form.Form;
import starter.ui.login.LoginForm;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class DoFormData implements Task {
    private final String desde;
    private final String hacia;

    public DoFormData(String desde, String hacia) {
        this.desde = desde;
        this.hacia = hacia;
    }

    public static Performable withFormData(String desde, String hacia) {
        return instrumented(DoFormData.class, desde, hacia);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(Form.INPUTDESDE),
                Enter.theValue(desde).into(Form.INPUTDESDE),
                Click.on(Form.SELECTDESDE),
                Click.on(Form.INPUTHACIA),
                Enter.theValue(hacia).into(Form.INPUTHACIA),
                Click.on(Form.SELECTHACIA),
                Click.on(Form.CALENDAR),
                Click.on(Form.DAYCALENDAR),
                Click.on(Form.PASSENGERS),
                Click.on(Form.ADULTS),
                Click.on(Form.CHILDREN),
                Click.on(Form.BABYS),
                Click.on(Form.CONTINUE),
                Click.on(Form.SEARCH)
        );
    }
}

package starter.tasks.blog;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import starter.tasks.blog.DoBlog;
import starter.ui.blogs.BlogsForm;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class DoBlog implements Task {

    private final String title_post;
    private final String permalink;
    private final String cuerpo_blog;
    private final String category;
    private final String related_post;
    private final String keywords;
    private final String description;
    private final String thumbnail;


    public DoBlog(String title_post, String permalink, String cuerpo_blog, String category,String related_post, String keywords, String description, String thumbnail) {
        this.title_post = title_post;
        this.permalink = permalink;
        this.cuerpo_blog = cuerpo_blog;
        this.category = category;
        this.related_post = related_post;
        this.keywords = keywords;
        this.description = description;
        this.thumbnail = thumbnail;

    }

    public static Performable withBlogs(String title_post, String permalink, String cuerpo_blog, String category,String related_post, String keywords, String description, String thumbnail) {
        return instrumented(DoBlog.class, title_post, permalink,cuerpo_blog, category, related_post , keywords, description, thumbnail);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(title_post).into(BlogsForm.TITLE_POST),
                Enter.theValue(permalink).into(BlogsForm.PERMALINK),
                SelectFromOptions.byVisibleText((category)).from(BlogsForm.CATEGORY),
                Enter.theValue(related_post).into(BlogsForm.RELATED_POST),
                Click.on(BlogsForm.ACCEPT_POST),
                Enter.theValue(thumbnail).into(BlogsForm.THUMBNAIL),
                Click.on(BlogsForm.SOURCE),
                Enter.theValue(cuerpo_blog).into(BlogsForm.CUERPO_BLOG),
                Enter.theValue(keywords).into(BlogsForm.KEYWORDS),
                Enter.theValue(description).into(BlogsForm.DESCRIPTION),
                Click.on(BlogsForm.BUTTON_SAVE)

        );
    }
}

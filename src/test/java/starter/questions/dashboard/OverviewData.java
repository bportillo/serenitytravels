package starter.questions.dashboard;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;
import starter.ui.dashboard.DashboardPage;
//import starter.ui.dashboard.LeftMenu;

public class OverviewData {

    public static Question<String> categoryCreate(){
        return actor -> TextContent.of(DashboardPage.LEFT_MENU.CATEGORY_CREATED).viewedBy(actor).asString().trim();
    }


}

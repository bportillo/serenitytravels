package starter.questions.dashboard;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.questions.TextContent;
import starter.ui.form.Form;

public class ViewData {

    public static Question<? extends String> SelectDate(){
        return actor -> TextContent.of(Form.INPUTDATESELECT).viewedBy(actor).asString().trim();
    }


}

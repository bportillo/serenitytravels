package starter.ui.blogs;

import org.openqa.selenium.By;

public class BlogsForm {
    public static By TITLE_POST = By.xpath("//*[@id=\"GENERAL\"]/div[1]/div[1]/div/input");
    public static By PERMALINK = By.xpath("//*[@id=\"GENERAL\"]/div[1]/div[2]/div/input");

    public static By CATEGORY = By.xpath("//*[@id=\"content\"]/form/div[2]/div/div/div[2]/div[2]/div/select");
    public static By RELATED_POST = By.xpath("//*[@id=\"s2id_autogen2\"]");
    public static By KEYWORDS = By.xpath("//*[@id=\"GENERAL\"]/div[3]/div/div/div[2]/div[1]/div/input");
    public static By DESCRIPTION = By.xpath("//*[@id=\"GENERAL\"]/div[3]/div/div/div[2]/div[2]/div/input");
    public static By THUMBNAIL = By.xpath("//*[@id=\"image_default\"]");
    public static By ACCEPT_POST = By.xpath("//*[@id=\"select2-drop\"]/ul/li");
    public static By BUTTON_SAVE = By.xpath("//*[@id=\"content\"]/form/div[1]/div/div[2]/button");

    public static By SOURCE = By.xpath("//*[@id=\"cke_46\"]");
    public static By CUERPO_BLOG = By.xpath("//*[@id=\"cke_1_contents\"]/textarea");

}

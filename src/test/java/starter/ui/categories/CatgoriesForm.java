package starter.ui.categories;

import org.openqa.selenium.By;

public class CatgoriesForm {
    public static By NAME_CATEGORY = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[2]/div[1]/div/input");
    public static By NAME_VIETNAMESE = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[2]/div[3]/div/input");
    public static By NAME_RUSSIAN = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[2]/div[4]/div/input");
    public static By NAME_ARABIC = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[2]/div[5]/div/input");
    public static By NAME_FARSI = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[2]/div[6]/div/input");
    public static By NAME_TURKISH = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[2]/div[7]/div/input");
    public static By NAME_FRENCH = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[2]/div[8]/div/input");
    public static By NAME_SPANISH = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[2]/div[9]/div/input");
    public static By NAME_GERMAN = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[2]/div[10]/div/input");
    public static By BUTTON_SAVE = By.xpath("//*[@id=\"ADD_BLOG_CAT\"]/div[2]/div/form/div[3]/button[2]");

}

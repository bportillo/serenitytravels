package starter.ui.dashboard;

import org.openqa.selenium.By;

public class LeftMenu {

    public static By BLOG = By.xpath("//*[@id=\"social-sidebar-menu\"]/li[14]/a");
    public static By BLOG_CATEGORIES = By.xpath("//*[@id=\"Blog\"]/li[2]/a");
    public static By ADD_CATEGORIES= By.xpath("//*[@id=\"content\"]/div[2]/div[2]/div[1]/button");

    public static By BLOG_POSTS = By.xpath("//*[@id=\"Blog\"]/li[1]/a");
    public static By ADD_BLOG= By.xpath("//*[@id=\"content\"]/div[2]/form/button");

    public static By CATEGORY_CREATED = By.xpath("//*[@id=\"content\"]/div[2]/div[2]/div[2]/div/div[1]/div[2]/table/tbody/tr[1]/td[3]");
}

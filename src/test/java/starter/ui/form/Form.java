package starter.ui.form;

import org.openqa.selenium.By;

public class Form {
    public static By TYPEFLIGHT = By.xpath("//*[@id=\"tab-navigation-1\"]/li[2]");
    public static By INPUTDESDE = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[1]/fieldset/div/div[1]/div/label/div/input[1]");
    public static By SELECTDESDE = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[1]/fieldset/div/div[1]/div/div/ul/li[1]");
    public static By INPUTHACIA = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[1]/fieldset/div/div[3]/div[2]/label/div/input[1]");
    public static By SELECTHACIA = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[1]/fieldset/div/div[3]/div[2]/div/ul/li[1]");
    public static By CALENDAR = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[2]/fieldset/div/div/div[1]/label/div/span/i");
    public static By DAYCALENDAR = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[2]/fieldset/div/div/div[3]/div[1]/table/tbody/tr/td[2]/div[3]/table/tbody/tr[2]/td[2]");
    public static By PASSENGERS = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[3]/fieldset/div/div[1]/div[1]/label/div/span/i");
    public static By ADULTS = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[3]/fieldset/div/div[1]/div[2]/div[2]/div[2]/div[3]/i");
    public static By CHILDREN = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[3]/fieldset/div/div[1]/div[2]/div[3]/div[2]/div[3]/i");
    public static By BABYS = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[3]/fieldset/div/div[1]/div[2]/div[4]/div[2]/div[3]/i");
    public static By CONTINUE = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[3]/fieldset/div/div[1]/div[2]/button");
    public static By SEARCH = By.xpath("/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/div/section/div[3]/div[4]/div[2]/div/form/div/div[2]/div/div/div[3]/fieldset/div/div[4]/button");

    public static By DATESELECT = By.xpath("//*[@id=\"dates-container\"]/button[8]");
    public static By INPUTDATESELECT = By.xpath("//*[@id=\"dates-container\"]/button[9]/div[1]");
}

package starter.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.locators.WaitForWebElements;
import org.openqa.selenium.support.ui.Wait;
import starter.navigation.NavigateTo;
import starter.questions.dashboard.OverviewData;
import starter.tasks.blog.DoBlog;
import starter.tasks.categories.DoCategories;
import starter.tasks.login.DoLogin;
import starter.ui.dashboard.DashboardPage;

import java.nio.file.Path;
import java.nio.file.Paths;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.core.IsEqual.equalTo;

public class CreateBlogStepDefinitions {

    String name;
    String name_category = "test";

    @Given("(.*) has account in the page")
    public void admin_has_account_in_the_page(String name) {
        // Write code here that turns the phrase above into concrete actions
        this.name = name;
    }

    @When("he sends their valid credentials for the login")
    public void he_sends_their_valid_credentials_for_the_login() {
        // Write code here that turns the phrase above into concrete actions
        theActorCalled(name).attemptsTo(
                NavigateTo.theDuckDuckGoHomePage(),
                DoLogin.withCredentials("admin@phptravels.com", "demoadmin")
        );
    }

    @Then("he should have access to create the category")
    public void he_should_have_access_to_create_the_category() {

        String name_vietnamese = "test";
        String name_russian = "test";
        String name_arabic = "test";
        String name_farsi = "test";
        String name_turkish = "test";
        String name_french = "test";
        String name_spanish = "test";
        String name_german = "test";

        theActorInTheSpotlight().attemptsTo(
                Click.on(DashboardPage.LEFT_MENU.BLOG),
                Click.on(DashboardPage.LEFT_MENU.BLOG_CATEGORIES)

        );
        theActorInTheSpotlight().attemptsTo(
                Click.on(DashboardPage.LEFT_MENU.ADD_CATEGORIES)
        );

        theActorInTheSpotlight().attemptsTo(
                DoCategories.withCategory(name_category,name_vietnamese,name_russian,name_arabic,name_farsi,name_turkish,name_french,name_spanish,name_german)
        );

        theActorInTheSpotlight().should(
                seeThat("The category create", OverviewData.categoryCreate(), equalTo(name_category))
        );
    }

    @Then("he should create blog")
    public void he_should_create_blog() {
        Path path = Paths.get("C:\\Users\\BryanAndresPortilloL\\Documents\\Personal\\Test\\Casos\\prueba.txt");
        String title_post = "prueba de test";
        String permalink = "prueba";
        String body_blog = "Esto es una prueba para la creación de un blog";
        String related_post = "Where";
        String keywords = "Prueba";
        String description = "esto es una prueba de la descripción";

        theActorInTheSpotlight().attemptsTo(
                Click.on(DashboardPage.LEFT_MENU.BLOG),
                Click.on(DashboardPage.LEFT_MENU.BLOG_POSTS),
                Click.on(DashboardPage.LEFT_MENU.ADD_BLOG),
                DoBlog.withBlogs(title_post,permalink,body_blog,name_category,related_post,keywords,description, path.toString())
        );
    }


}

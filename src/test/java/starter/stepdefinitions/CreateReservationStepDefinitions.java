package starter.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.waits.WaitUntil;
import starter.navigation.NavigateTo;
import starter.questions.dashboard.OverviewData;
import starter.questions.dashboard.ViewData;
import starter.tasks.login.DoLogin;
import starter.tasks.typeflight.DoDateFlight;
import starter.tasks.typeflight.DoFormData;
import starter.tasks.typeflight.DoTypeFlight;

import javax.swing.text.View;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.core.IsEqual.equalTo;

public class CreateReservationStepDefinitions {

    String name;

    @Given("(.*) has enter the page")
    public void user_has_enter_the_page(String name) {
        // Write code here that turns the phrase above into concrete actions
        this.name = name;
        theActorCalled(name).attemptsTo(
                NavigateTo.theDuckDuckGoHomePage()
        );
    }

    @When("he select the type flight")
    public void he_select_the_type_flight() {
        // Write code here that turns the phrase above into concrete actions
        theActorCalled(name).attemptsTo(
                DoTypeFlight.withtype()
        );
        //throw new cucumber.api.PendingException();
    }

    @Then("he fills in all the flight information")
    public void he_fills_in_all_the_flight_information() {
        // Write code here that turns the phrase above into concrete actions
        theActorCalled(name).attemptsTo(
                DoFormData.withFormData("medellin","bogota")
        );
    }

    @Then("he select the flight")
    public void he_select_the_flight() {
        // Write code here that turns the phrase above into concrete actions
        String dateselected = "LUN 08 FEB";

        theActorCalled(name).attemptsTo(
                DoDateFlight.withDate()
        );

        theActorInTheSpotlight().should(
                //seeThat("The Date selected", ViewData.SelectDate())
                seeThat("The Date selected" , ViewData.SelectDate(), equalTo(dateselected))
        );


    }

}

Feature: Create blog into the application

  @blog
  Scenario: Successful create blog
    Given admin has account in the page
    When he sends their valid credentials for the login
    Then he should have access to create the category
    And he should create blog